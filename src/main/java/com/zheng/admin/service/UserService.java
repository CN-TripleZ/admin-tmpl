package com.zheng.admin.service;

import com.zheng.admin.entity.PermSubject;
import com.zheng.admin.form.PagingReq;
import com.zheng.admin.form.PagingResp;
import com.zheng.admin.model.Perm;
import com.zheng.admin.model.Role;
import com.zheng.admin.model.User;

import java.util.List;

/**
 * Created by zcz on 2017/10/26.
 */
public interface UserService {

    User getUserByPhone(String phone);

    Integer createUser(User user);

    Integer updateUser(User user);

    Integer createRole(Role role);

    Integer updateRole(Role role);

    Integer createPerm(Perm perm);

    Integer updatePerm(Perm perm);

    Integer deleteRoleByIds(Integer... ids);

    Integer deletePermByIds(Integer... ids);

    User getUserById(Integer id);

    Integer deleteUserByIds(Integer... ids);

    Integer deleteUserRole(Integer userId, Integer roleId);

    Integer addUserRole(Integer userId, Integer roleId);

    Role getRoleById(Integer id);

    List<Role> queryAllRoles();

    List<Role> queryUserRoles(Integer userId);

    PermSubject buildUserPermSubject(Integer userId);

    PagingResp<User> queryUserByPaging(PagingReq req);

    List<Role> queryRoleByUserId(Integer userId);

    PagingResp<Role> queryRoleByPaging(PagingReq req);

    Perm getPermById(Integer id);

    PagingResp<Perm> queryPermByPaging(PagingReq req);

    PagingResp<Perm> queryRolePermByPaging(Integer roleId, PagingReq req);

    PagingResp<Role> queryPermRoleByPaging(Integer permId, PagingReq req);

    PagingResp<Perm> queryUserPermByPaging(Integer userId, PagingReq req);

    Integer deleteRolePerm(Integer permId, Integer roleId);

    Integer addRolePerm(Integer permId, Integer roleId);

    List<Integer> queryRolePermIds(Integer roleId);

    List<Integer> queryPermRoleIds(Integer permId);

    PagingResp<User> queryRoleUserByPaging(Integer roleId, PagingReq req);

    List<Integer> queryRoleUserIds(Integer roleId);
}
