package com.zheng.admin.model;

import org.apache.shiro.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by zcz on 2017/10/25.
 */
public class Perm {

    private Integer id;

    private String type;

    private String name;

    private String info;

    private String status;

    private String permString;

    private List<Role> roles;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public String getPermString() {
        if (permString != null) {
            return permString;
        } else {
            if (name == null || name.trim().length() == 0) {
                return "";
            }
            return type + PART_DIVIDER_TOKEN + name.trim().toLowerCase();
        }
    }

    public void setPermString(String permString) {
        this.permString = permString;
        initParts();
    }

    protected static final String WILDCARD_TOKEN = "*";
    protected static final String PART_DIVIDER_TOKEN = ":";
    protected static final String SUBPART_DIVIDER_TOKEN = ",";

    public Perm() {

    }

    private List<Set<String>> parts;

    public void setParts(List<Set<String>> parts) {
        this.parts = parts;
    }

    public void initParts() {
        List<String> parts = CollectionUtils.asList(getPermString().split(PART_DIVIDER_TOKEN));

        this.parts = new ArrayList<Set<String>>();
        for (String part : parts) {
            Set<String> subparts = CollectionUtils.asSet(part.split(SUBPART_DIVIDER_TOKEN));

            if (!subparts.isEmpty()) {
                this.parts.add(subparts);
            }
        }
    }

    public List<Set<String>> getParts() {
        return this.parts;
    }

    public boolean isPermitted(String permissionName) {
        Perm otherPermission = new Perm();
        otherPermission.setPermString(permissionName);
        List<Set<String>> otherParts = otherPermission.getParts();
        int i = 0;
        for (Set<String> otherPart : otherParts) {
            if (getParts().size() - 1 < i) {
                return true;
            } else {
                Set<String> part = getParts().get(i);
                if (!part.contains(WILDCARD_TOKEN) && !part.containsAll(otherPart)) {
                    return false;
                }
                i++;
            }
        }

        for (; i < getParts().size(); i++) {
            Set<String> part = getParts().get(i);
            if (!part.contains(WILDCARD_TOKEN)) {
                return false;
            }
        }

        return true;
    }

    @Override
    public String toString() {
        return "[" + id + "]" + "[" + type + "]" + name;
    }
}
