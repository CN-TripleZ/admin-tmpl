package com.zheng.admin.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by zcz on 2017/10/25.
 */
@Controller
public class IndexController {

    @RequestMapping("/index.html")
    public String index(ModelMap modelMap) {
        return "index";
    }
}
