package com.zheng.admin.form;

/**
 * Created by zcz on 2017/10/31.
 */
public class PagingSearch {

    private String value;

    private Boolean regex;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Boolean getRegex() {
        return regex;
    }

    public void setRegex(Boolean regex) {
        this.regex = regex;
    }
}
