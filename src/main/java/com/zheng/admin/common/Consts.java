package com.zheng.admin.common;

/**
 * Created by zcz on 2017/10/26.
 */
public interface Consts {

    String SESS_USER_ID = "session.user.id";

    String SESS_PERM_SUB = "session.perm.sub";
}
