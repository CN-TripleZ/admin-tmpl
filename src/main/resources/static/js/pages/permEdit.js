$.App.Page.PermRoleList = {
    "columns" : [
        { "data": "id", "title": "ID", "orderable": false  },
        { "data": "name", "title": "名称", "orderable": true },
        { "data": "info", "title": "描述", "orderable": false},
        { "data": "opt", "title": "操作", "orderable": false }
    ],

    "columnDefs" : [
        {
            "targets": [-1], // 目标列位置，下标从0开始
            'className': 'dt-center',
            "width" : 80,
            "render": function(data, type, row) { // 返回自定义内容
               return '<a class="btn btn-xs btn-info" href="#/role/' + row.id + '.html">编辑</a>' +
                      '<a class="btn btn-xs btn-danger btn-row-remove" href="javascript:void(0);" data-id="' + row.id +'">删除</a>';
            }
        }
    ],

    "drawCallback" : function(settings) {

    }
};

$.App.Page.PermRoleSelectList = {
    "columns" : [
        { "data": "id", "title": "ID", "orderable": false  },
        { "data": "name", "title": "名称", "orderable": true },
        { "data": "info", "title": "描述", "orderable": false},
        { "data": "opt", "title": "操作", "orderable": false }

    ],

    "columnDefs" : [
        {
            "targets": [-1], // 目标列位置，下标从0开始
            'className': 'dt-center',
            "width" : 30,
            "render": function(data, type, row) { // 返回自定义内容
               return '<input type="checkbox" name="rp-row-chk" data-id="' + row.id + '"/>';
            }
        }
    ],
    "drawCallback" : function(settings) {
        $('#PermRoleSelectList input[type=checkbox]').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            increaseArea: '10%'
        });
        var permId = $("#id").val();
        $.ajax({
            url: '/role/queryPermRoleIds?permId=' + permId,
            type:"POST",
            contentType: "application/json",
            async : false,
            success : function(data){
                $.each(data, function( index, value ) {
                    $("#PermRoleSelectList input[type=checkbox][data-id=" + value +"]").iCheck("check");
                });
            },
            error : function(){
                $.App.notify("连接失败,请稍候再试!", {type: "error"});
            }
        });
    }
};

$(document).ready(function() {
    $(".add_btn").on("click", function() {
        $("#addRoleModal").modal("show");
    });
    $("#addRoleModal").on("hidden.bs.modal", function(e){
        $.App.Page['PermRoleList'].draw();
    });
    $("#addPermModal").on("show.bs.modal", function(e){
        $.App.Page['PermRoleSelectList'].draw();
    });
    $("#addRoleModal").on("ifClicked", "input[type=checkbox][name=rp-row-chk]", function(event) {
        var self = $(this);
        var isChecked = self.prop("checked");
        var permId = $("#id").val();
        var roleId = self.data("id");
        var url = "";
        if (isChecked) {
            url = "/role/deleteRolePerm?permId=" + permId + "&roleId=" + roleId;
        } else {
             url = "/role/addRolePerm?permId=" + permId + "&roleId=" + roleId;
        }
        $.ajax({
            url: url,
            type:"POST",
            contentType: "application/json",
            async : false,
            success : function(resp){
                if ('success' === resp.code) {
                    $.App.notify(resp.message);
                } else {
                    setTimeout(function(){
                        self.iCheck(isChecked ? "check" : "uncheck");
                    }, 200);
                    $.App.notify(resp.message, {type: "error"});
                }
            },
            error : function(){
                setTimeout(function(){
                        self.iCheck(isChecked ? "check" : "uncheck");
                }, 200);
                $.App.notify("连接失败,请稍候再试!", {type: "error"});
            }
        });
    });
    $("#PermRoleList").on("click", ".btn-row-remove", function() {
        var permId = $("#id").val();
        var roleId = $(this).data("id");
        $.ajax({
            url: "/role/deleteRolePerm?permId=" + permId + "&roleId=" + roleId,
            type:"POST",
            contentType: "application/json",
            async : false,
            success : function(resp){
                if ('success' === resp.code) {
                    $.App.notify(resp.message);
                    $.App.Page['PermRoleList'].draw();
                } else {
                    $.App.notify(resp.message, {type: "error"});
                }
            },
            error : function(){
                $.App.notify("连接失败,请稍候再试!", {type: "error"});
            }
        });
    });
    $(".update-btn").on("click", function() {
        $.ajax({
            type: "POST",
            url:"/perm/save",
            data: JSON.stringify($('#updatePermForm').serializeObject()),
            contentType: "application/json",
            dataType: "json",
            async: false,
            error: function(request) {
                $.App.notify('连接失败,请稍候再试!', {type: "error"});
            },
            success: function(resp) {
                if ('success' === resp.code) {
                    $.App.notify(resp.message);
                } else {
                    $.App.notify(resp.message, {type: "error"});
                }
            }
        });
    });
});