$.App.Page.UserPermList = {
    "columns" : [
        { "data": "id", "title": "ID", "orderable": false  },
        { "data": "type", "title": "类型", "orderable": false },
        { "data": "name", "title": "名称", "orderable": false},
        { "data": "info", "title": "描述", "orderable": false }
    ],

    "columnDefs" : [

    ],

    "drawCallback" : function(settings) {

    }
};

$(document).ready(function() {
    $('input[type=checkbox][name=role-row-chk]').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        increaseArea: '10%'
    });
    $('#role-modal-list').dataTable();
    $(".update-btn").on("click", function() {
        $.ajax({
            type: "POST",
            url:"/user/save",
            data: JSON.stringify($('#updateUserForm').serializeObject()),
            contentType: "application/json",
            dataType: "json",
            async: false,
            error: function(request) {
                $.App.notify('连接失败,请稍候再试!', {type: "error"});
            },
            success: function(resp) {
                if ('success' === resp.code) {
                    $("#addModal").modal("hide");
                    $.App.notify(resp.message);
                    setTimeout(function() {
                        $.App.goPage('/user/' + resp.data.userId + ".html");
                    }, 1000);
                } else {
                    $.App.notify(resp.message, {type: "error"});
                }
            }
        });
    });
    $("#updateUserForm").on("click", ".role-rm-btn", function(event) {
        event.stopPropagation();
        event.preventDefault();
        var self = $(this);
        var roleId = self.data("roleid");
        var userId = self.data("userid");
        $.App.alert('警告', '确定要删除吗?', function(){
            $.ajax({
                url:"/user/deleteUserRole?userId=" + userId + "&roleId=" + roleId,
                type:"POST",
                contentType: "application/json",
                async : false,
                success : function(resp){
                    if ('success' === resp.code) {
                        $.App.notify(resp.message);
                        $.App.reload();
                    } else {
                        $.App.notify(resp.message, {type: "error"});
                    }
                },
                error : function(){
                    $.App.notify("连接失败,请稍候再试!", {type: "error"});
                }
            });
        });
    });
    $(document).on("click", ".add_role_btn", function() {
        $("#addRoleModal").modal("show");
        $(".role-rm-btn").each(function(){
            var self = $(this);
            console.log(self)
            $("input[type=checkbox][name=role-row-chk][data-roleid=" + self.data("roleid") +"]").iCheck("check");
        });
    });
    $('#addRoleModal').on('hidden.bs.modal', function () {
        $.App.reload();
    });
//    $("#addRoleModal").on("ifChecked", "input[type=checkbox][name=role-row-chk]", function() {
//        var self = $(this);
//        var roleId = self.data("roleid");
//        var userId = self.data("userid");
//        $.ajax({
//            url:"/user/addUserRole?userId=" + userId + "&roleId=" + roleId,
//            type:"POST",
//            contentType: "application/json",
//            async : false,
//            success : function(resp){
//                if ('success' === resp.code) {
//                    $.App.notify(resp.message);
//                } else {
//                    $.App.notify(resp.message, {type: "error"});
//                }
//            },
//            error : function(){
//                $.App.notify("连接失败,请稍候再试!", {type: "error"});
//            }
//        });
//    });

    $("#addRoleModal").on("ifClicked", "input[type=checkbox][name=role-row-chk]", function(event) {
        var self = $(this);
        var isChecked = self.prop("checked");
        var roleId = self.data("roleid");
        var userId = self.data("userid");
        var url = "";
        if (isChecked) {
            url = "/user/deleteUserRole?userId=" + userId + "&roleId=" + roleId;
        } else {
             url = "/user/addUserRole?userId=" + userId + "&roleId=" + roleId;
        }
        $.ajax({
            url: url,
            type:"POST",
            contentType: "application/json",
            async : false,
            success : function(resp){
                if ('success' === resp.code) {
                    $.App.notify(resp.message);
                } else {
                    console.log(isChecked);
                    setTimeout(function(){
                        self.iCheck(isChecked ? "check" : "uncheck");
                    }, 200);
                    $.App.notify(resp.message, {type: "error"});
                }
            },
            error : function(){
                setTimeout(function(){
                        self.iCheck(isChecked ? "check" : "uncheck");
                }, 200);
                $.App.notify("连接失败,请稍候再试!", {type: "error"});
            }
        });
    });
});