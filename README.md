# 管理后台模板
管理后台模板。
前端使用：Admin-LTE
后端使用：Spring Boot
支持最基本的权限。主要是方便下次直接使用。

角色管理
![输入图片说明](https://gitee.com/uploads/images/2017/1106/165732_5a1706d0_61595.png "role.png")
![输入图片说明](https://gitee.com/uploads/images/2017/1106/165848_7198369b_61595.png "role_edit.png")

用户管理
![输入图片说明](https://gitee.com/uploads/images/2017/1106/165934_6ab7f5d0_61595.png "user.png")
![输入图片说明](https://gitee.com/uploads/images/2017/1106/170030_2678ae4a_61595.png "user_edit.png")

权限管理
![输入图片说明](https://gitee.com/uploads/images/2017/1106/170121_6265bf3a_61595.png "perm.png")
![输入图片说明](https://gitee.com/uploads/images/2017/1106/170130_37277b9e_61595.png "perm_edit.png")

支持menu和opt两种权限。通常.html结束代表一个html页面。非html结束代表一个json数据请求。