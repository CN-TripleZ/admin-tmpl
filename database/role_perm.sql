SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for role_perm
-- ----------------------------
DROP TABLE IF EXISTS `role_perm`;
CREATE TABLE `role_perm` (
  `roleId` int(11) NOT NULL,
  `permId` int(11) NOT NULL,
  PRIMARY KEY (`roleId`,`permId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role_perm
-- ----------------------------
INSERT INTO `role_perm` VALUES ('1', '1');
INSERT INTO `role_perm` VALUES ('1', '2');
INSERT INTO `role_perm` VALUES ('1', '3');
INSERT INTO `role_perm` VALUES ('1', '4');
INSERT INTO `role_perm` VALUES ('1', '5');
INSERT INTO `role_perm` VALUES ('1', '6');
INSERT INTO `role_perm` VALUES ('1', '7');
INSERT INTO `role_perm` VALUES ('1', '8');
INSERT INTO `role_perm` VALUES ('1', '9');
