SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for perm
-- ----------------------------
DROP TABLE IF EXISTS `perm`;
CREATE TABLE `perm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(16) NOT NULL,
  `name` varchar(128) NOT NULL,
  `info` varchar(256) DEFAULT NULL,
  `status` varchar(16) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of perm
-- ----------------------------
INSERT INTO `perm` VALUES ('1', 'menu', 'user.html', '用户列表', 'normal');
INSERT INTO `perm` VALUES ('2', 'menu', 'role.html', '角色列表', 'normal');
INSERT INTO `perm` VALUES ('3', 'menu', 'perm.html', '权限列表', 'normal');
INSERT INTO `perm` VALUES ('4', 'menu', 'user:*', '用户编辑', 'normal');
INSERT INTO `perm` VALUES ('5', 'menu', 'role:*', '角色编辑', 'normal');
INSERT INTO `perm` VALUES ('6', 'menu', 'perm:*', '权限编辑', 'normal');
INSERT INTO `perm` VALUES ('7', 'opt', 'user:*', '用户管理', 'normal');
INSERT INTO `perm` VALUES ('8', 'opt', 'role:*', '角色管理', 'normal');
INSERT INTO `perm` VALUES ('9', 'opt', 'perm:*', '权限管理', 'normal');
